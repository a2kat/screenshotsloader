﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormScreenshotsLoader
{
	public partial class Form1 : Form
	{ 
		public string folder =  @"Snapshots";
		public List<string> screenshots = new List<string>();
		public int currentScreen = 0;
		public Form1()
		{
			InitializeComponent();
			FindScreens();
			this.Width = 566;
			this.Height = 1040;
			
			this.pb.Size = new Size(552,1020);
			pb.Image = new Bitmap($@"{screenshots[currentScreen]}");
			
			Rectangle r = Screen.FromControl(this).WorkingArea;
			var width = Screen.PrimaryScreen.Bounds.Width;
			this.Bounds = new Rectangle(width-559 , 0, this.Width, r.Height);
		}

		private void FindScreens()
		{
			var files  = Directory.GetFiles(folder);
			screenshots.AddRange(files);
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Rectangle r = Screen.FromControl(this).WorkingArea;
			var width = Screen.PrimaryScreen.Bounds.Width;
			this.Bounds = new Rectangle(width-559 , 0, this.Width, r.Height);
		}

		private void button2_Click(object sender, EventArgs e)
		{
			// Back.
			currentScreen--;
			if (currentScreen < 0)
			{
				currentScreen = screenshots.Count - 1;
			}
			pb.Image = new Bitmap($@"{screenshots[currentScreen]}");
		}

		private void button3_Click(object sender, EventArgs e)
		{
			// Forward.
			currentScreen++;
			if (currentScreen > screenshots.Count - 1)
			{
				currentScreen = 0;
			}
			pb.Image = new Bitmap($@"{screenshots[currentScreen]}");
		}
	}
}